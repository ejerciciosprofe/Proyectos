package Casino;

public class Banca{
	
	static int dineroBanca = 3000;
	Jugador jugador;
	int numGanador;
	
	public int getNumGanador() {
		return numGanador;
	}

	public void setNumGanador(int numGanador) {
		this.numGanador = numGanador;
	}

	public static int getDineroBanca() {
		return dineroBanca;
	}

	public static void setDineroBanca(int dineroBanca) {
		Banca.dineroBanca = dineroBanca;
	}
	
	public int generarNumero() {
		return (int)(Math.random()*36+1);
	}
	
	protected synchronized int juego(int dinero, String nombre, int num, boolean resultados) {
		if(dinero >= 1 || dineroBanca >= 1) {
			dinero-=10;
			dineroBanca += 10;
			System.out.println(nombre+" ha apostado 10 euros al "+num+" su saldo actual es de "+dinero);
			dinero = girarRuleta(dinero,nombre,num);
		}
		return dinero;
	}
	
	protected int getResultados(int dinero, String nombre, int num) {
		if(num == numGanador) {
			dinero = resultados(true, nombre, dinero);
		}else {
			dinero = resultados(false, nombre, dinero);
		}
		return dinero;
	}
	
	protected synchronized int girarRuleta(int dinero, String nombre, int num) {
		try {
			wait();
			dinero = getResultados(dinero,nombre,num);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return dinero;
	}
	
	protected int resultados(boolean ganador, String nombre, int dinero) {
		if(ganador) {
			dinero += 360;
			dineroBanca -= 360;
			System.out.println(nombre+" ha ganado OLEEEE, su saldo ahora es de "+dinero);
		}else {
			System.out.println(nombre+" ha perdido :(");
		}
		return dinero;
	}
	
	protected synchronized void avisar() {
		if(dineroBanca>=361) {
			System.out.println("Aqui estan los resultados, el n�mero ganador era el: "+numGanador);
			notifyAll();
		}else {
			System.out.println("La banca no tiene dinero");
		}
	}

}
