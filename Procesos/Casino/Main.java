package Casino;

public class Main {

	public static void main(String[] args) {
		
		Banca b = new Banca();
		
		Jugador j1 = new Jugador(300,"Carlos",b);
		Jugador j2 = new Jugador(300,"Juan",b);
		Jugador j3 = new Jugador(300,"Maria",b);
		Jugador j4 = new Jugador(20,"Nicolas",b);
		Jugador j0 = new Jugador(3000,"Banca",b);
		
		j0.start();
		j1.start();
		j2.start();
		j3.start();
		j4.start();
	}

}
