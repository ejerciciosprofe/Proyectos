package Casino;

public class Jugador extends Thread{
	
	int dinero;
	int num;
	int numGanador;
	boolean ganador;
	String nombre;
	Banca banca;
	
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getNumGanador() {
		return numGanador;
	}

	public void setNumGanador(int numGanador) {
		this.numGanador = numGanador;
	}

	public Jugador(int dinero, String nombre, Banca banca) {
		this.dinero = dinero;
		this.nombre = nombre;
		this.banca = banca;
	}

	public int getDinero() {
		return dinero;
	}

	public void setDinero(int dinero) {
		this.dinero = dinero;
	}

	@Override
	public void run() {
		while(dinero >= 1 && banca.dineroBanca>=361) {
			if(nombre.equalsIgnoreCase("Banca")) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				int numGanador = banca.generarNumero();
				banca.setNumGanador(numGanador);
				banca.avisar();
			}else {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				num = banca.generarNumero();
				dinero = banca.juego(dinero,nombre,num,false);
			}
		}
	}
	
}
