package calculadora;

public class Operaciones {

    public Operaciones() {

    }

    public static boolean isNumeric(String cadena) {
        boolean resultado;

        try {
            Double.parseDouble(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }

    public String cadena(String[] op, String operador, double resultado) {

        String cadenaFinal = "";

        cadenaFinal += op[1] + " ";

        for (int i = 2; i < op.length; i++) {
            cadenaFinal += operador + " ";
            cadenaFinal += op[i] + " ";
        }

        cadenaFinal += "= ";
        cadenaFinal += resultado;

        return cadenaFinal;
    }

    protected String suma(String[] op) {
        if (!isNumeric(op[1])) {
            return "n";
        } else {
            double resultado = Double.parseDouble(op[1]);

            for (int i = 2; i < op.length; i++) {
                if (!isNumeric(op[i])) {
                    return "n";
                } else {
                    resultado += Double.parseDouble(op[i]);
                }
            }

            String cadenaFinal = "";

            cadenaFinal = cadena(op, "+", resultado);

            return cadenaFinal;
        }
    }

    protected String resta(String[] op) {
        if (!isNumeric(op[1])) {
            return "n";
        } else {
            double resultado = Double.parseDouble(op[1]);

            for (int i = 2; i < op.length; i++) {
                if (!isNumeric(op[i])) {
                    return "n";
                } else {
                    resultado -= Double.parseDouble(op[i]);
                }
            }

            String cadenaFinal = "";

            cadenaFinal = cadena(op, "-", resultado);

            return cadenaFinal;
        }
    }

    protected String mult(String[] op) {
        if (!isNumeric(op[1])) {
            return "n";
        } else {
            double resultado = Double.parseDouble(op[1]);

            for (int i = 2; i < op.length; i++) {
                if (!isNumeric(op[i])) {
                    return "n";
                } else {
                    resultado *= Double.parseDouble(op[i]);
                }
            }

            String cadenaFinal = "";

            cadenaFinal = cadena(op, "*", resultado);

            return cadenaFinal;
        }
    }

    protected String div(String[] op) {
        if (!isNumeric(op[1])) {
            return "n";
        } else {
            double resultado = Double.parseDouble(op[1]);

            for (int i = 2; i < op.length; i++) {
                if (!isNumeric(op[i])) {
                    return "n";
                } else {
                    resultado /= Double.parseDouble(op[i]);
                }
            }

            String cadenaFinal = "";

            cadenaFinal = cadena(op, "/", resultado);

            return cadenaFinal;
        }
    }

}
