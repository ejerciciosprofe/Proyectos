package calculadora;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {

    public static void main(String args[]) throws IOException {

        ServerSocket servidor = null;
        Socket sc = null;
        Operaciones o = new Operaciones();
        ServerSocket serverSocket;

        DataInputStream in;
        DataOutputStream out;

        final String HOST = "localhost";
        final int PORT = 5000;

        serverSocket = new ServerSocket(PORT);

        sc = serverSocket.accept();

        in = new DataInputStream(sc.getInputStream());
        out = new DataOutputStream(sc.getOutputStream());

        out.writeUTF("Bienvenido a CALCULADORA EN UN SERVIDOR, ¿Qué quieres hacer hoy?\n \n"
                + "- - - MENU - - - \n \n"
                + "1 - Suma COMMAND = #SUMA num num num ... \n"
                + "2 - Resta COMMAND = #RESTA num num num ... \n"
                + "3 - Multiplicacion COMMAND = #MULT num num num ... \n"
                + "4 - Division COMMAND = #DIV num num num ... \n"
                + "5 - Terminar Conexion COMMAND = #SALIR");

        while (true) {

            String mensajeServidor = "El comando es incorrecto recuerda que es # "
                            + "con la operación que quieres y los numeros como #SUMA 1 1";

            // Recibo un mensaje y lo muestro
            String mensaje = in.readUTF();

            String[] partes = mensaje.split(" ");

            try {
                if (partes[0].equalsIgnoreCase("#SUMA")) {
                    mensajeServidor = o.suma(partes) + "";
                } else if (partes[0].equalsIgnoreCase("#RESTA")) {
                    mensajeServidor = o.resta(partes) + "";
                } else if (partes[0].equalsIgnoreCase("#MULT")) {
                    mensajeServidor = o.mult(partes) + "";
                } else if (partes[0].equalsIgnoreCase("#DIV")) {
                    mensajeServidor = o.div(partes) + "";
                } else if (partes[0].equalsIgnoreCase("#SALIR")) {
                    out.writeUTF("Gracias por usar CALCULADORA EN UN SERVIDOR, hasta la proxima");
                    break;
                } else {
                    mensajeServidor = "El comando es incorrecto recuerda que es # "
                            + "con la operación que quieres y los numeros como #SUMA 1 1";
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                out.writeUTF("El comando es incorrecto recuerda que es # "
                        + "con la operación que quieres y los numeros como #SUMA 1 1");
            }

            System.out.println(partes[0]);

            // Leer los mensajes que se envian
            if (mensajeServidor.equals("n")) {
                out.writeUTF("Uno de los datos no es un numero");
            }else if(mensajeServidor.charAt(mensajeServidor.length()-1) == ('y')){
                out.writeUTF("ERROR de calculo");
            } else {
                out.writeUTF(mensajeServidor);
            }

        }

    }
}
