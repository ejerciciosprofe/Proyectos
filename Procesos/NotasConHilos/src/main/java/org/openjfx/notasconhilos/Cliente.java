package org.openjfx.notasconhilos;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Cliente {

    public static void main(String args[]) {

        final String HOST = "127.0.0.1";
        final int PORT = 5000;

        Scanner kb = new Scanner(System.in);

        // Creo el socket para conectarme con el cliente
        try {

            DataInputStream in;
            DataOutputStream out;

            Socket sc = new Socket(HOST, PORT);

            in = new DataInputStream(sc.getInputStream());
            out = new DataOutputStream(sc.getOutputStream());
            
            System.out.println("- - - Conexion establecida - - - ");
            System.out.println("Escribiendo con "+HOST);

            while (true) {
                String mensaje = in.readUTF();
                System.out.println(mensaje);
                
                if(mensaje.equals("")){
                    break;
                }
                
                String envio = kb.nextLine();
                out.writeUTF(envio);
            }
        } catch (Exception e) {
            System.out.println("Conexión perdida");
        }
    }
}