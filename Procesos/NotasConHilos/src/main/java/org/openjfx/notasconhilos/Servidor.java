package org.openjfx.notasconhilos;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Servidor {

    protected static ArrayList<Tarea> tareas;

    public static void main(String args[]) throws IOException {

        ServerSocket servidor = null;
        Socket sc = null;
        ServerSocket serverSocket;

        tareas = new ArrayList<Tarea>();

        DataInputStream in;
        DataOutputStream out;

        final String HOST = "localhost";
        final int PORT = 5000;

        serverSocket = new ServerSocket(PORT);
        while (true) {
            try {
                sc = serverSocket.accept();

                in = new DataInputStream(sc.getInputStream());
                out = new DataOutputStream(sc.getOutputStream());
                Hilo h = new Hilo(in, out);

                h.start();
            } catch (Exception e) {
                System.err.println("Unexpected error");
            }
        }

    }

}
