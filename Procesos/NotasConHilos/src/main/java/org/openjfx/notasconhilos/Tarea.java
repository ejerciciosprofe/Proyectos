package org.openjfx.notasconhilos;

public class Tarea {
    
    protected Persona persona;
    protected String descripcion, dia;

    public Tarea(Persona persona, String descripcion, String dia) {
        this.persona = persona;
        this.descripcion = descripcion;
        this.dia = dia;
    }

    @Override
    public String toString() {
        return "Tarea{" + "persona=" + persona + ", descripcion=" + descripcion + ", dia=" + dia + '}';
    }
    
    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }
    
    
    
}
