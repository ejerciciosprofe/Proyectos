package org.openjfx.notasconhilos;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.openjfx.notasconhilos.Servidor.tareas;

public class Hilo extends Thread {

    private Scanner kb = new Scanner(System.in);

    private DataInputStream in;
    private DataOutputStream out;

    public Hilo(DataInputStream in, DataOutputStream out) {
        this.in = in;
        this.out = out;
    }

    @Override
    public void run() {

        try {
            out.writeUTF("¿Cual es tu nombre?");
            String nombre = in.readUTF();
            out.writeUTF("¿Cual es tu apellido?");
            String apellido = in.readUTF();
            out.writeUTF("¿Cual es tu edad?");
            String edad = in.readUTF();

            Persona p = new Persona(nombre, apellido, Integer.parseInt(edad));

            out.writeUTF("¿Que quieres hacer? \n1- Nueva tarea\n2- Ver tareas\n3- Salir");
            String opc = in.readUTF();
            System.out.println(opc);
            if (Integer.parseInt(opc) == 1) {
                out.writeUTF("¿Cual es la tarea que quieres añadir?");
                String tarea = in.readUTF();
                out.writeUTF("¿Que dia la quieres realizar DD/MM/YYYY?");
                String fecha = in.readUTF();

                Tarea t = new Tarea(p, tarea, fecha);
                tareas.add(t);
            } else if (Integer.parseInt(opc) == 2) {
                String datos = "";
                for (Tarea i : tareas) {
                    datos += i.toString();
                    datos += "\n";
                }
                out.writeUTF(datos);
            }
        } catch(Exception e){
            System.err.println("Unexpected error");
        }

    }
}
