package Monje;

public class Monje extends Thread{
	
	private boolean monjes[];
	private int id;
	private boolean comiendo;
	Monje i;
	Mesa mesa;
		
	public Monje(Mesa mesa, int id) {
		this.mesa = mesa;
		this.id = id;
		comiendo = false;
	}

	public boolean isComiendo() {
		return comiendo;
	}

	public void setComiendo(boolean comiendo) {
		this.comiendo = comiendo;
	}

	@Override
	public void run() {
			mesa.cogerTenedor(id);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			mesa.dejarDeComer(id);
		
		
	}
	
}
