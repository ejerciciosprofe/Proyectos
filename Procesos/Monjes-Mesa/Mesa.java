package Monje;

public class Mesa {
	
	private Monje monjes[];

	public Mesa(Monje[] monjes) {
		this.monjes = monjes;
	}
	
	protected synchronized void cogerTenedor(int id) {
		int  anterior;
		int siguiente;
		for (int i = 0; i < monjes.length; i++) {
			if(!monjes[id].isComiendo()) {
				if(id==0) {
					anterior = monjes.length-1;
					siguiente = id + 1;
					if(!monjes[anterior].isComiendo() && !monjes[siguiente].isComiendo()) {
						System.out.println("El monje "+id+" empieza a comer");
						monjes[id].setComiendo(true);
						break;
					}else {
						System.out.println("El monje "+id+" empieza a rezar");
						empiezaARezar(id);
					}
				} else if(id == monjes.length-1) {
					anterior = id - 1;
					siguiente = 0;
					if(!monjes[anterior].isComiendo() && !monjes[siguiente].isComiendo()) {
						System.out.println("El monje "+id+" empieza a comer");
						monjes[id].setComiendo(true);
						break;
					}else {
						System.out.println("El monje "+id+" empieza a rezar");
						empiezaARezar(id);
					}
				}else {
					anterior = id - 1;
					siguiente = id + 1;
					if(!monjes[anterior].isComiendo() && !monjes[siguiente].isComiendo()) {
						System.out.println("El monje "+id+" empieza a comer");
						monjes[id].setComiendo(true);
						break;
					}else {
						System.out.println("El monje "+id+" empieza a rezar");
						empiezaARezar(id);
					}
				}
			}
		}
	}
	
	protected synchronized void dejarDeComer(int id) {
		System.out.println("El monje "+id+" deja de comer");
		monjes[id].setComiendo(false);
		notifyAll();
	}
	
	protected synchronized void empiezaARezar(int id) {
		try {
			wait();
			cogerTenedor(id);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
