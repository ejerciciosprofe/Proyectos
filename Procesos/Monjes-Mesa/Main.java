package Monje;

public class Main {

	protected static Monje monjes[];
	
	
	public static void main(String[] args) {
	
		monjes = new Monje[5];

		
		Monje monje1;
		Monje monje2;
		Monje monje3;
		Monje monje4;
		Monje monje5;
		
		Mesa mesa = new Mesa(monjes);
		monjes[0] = monje1 = new Monje(mesa,0);
		monjes[1] = monje2 = new Monje(mesa,1);
		monjes[2] =monje3 = new Monje(mesa,2);
		monjes[3] = monje4 = new Monje(mesa,3);
		monjes[4] = monje5 = new Monje(mesa,4);
				
		monje1.start();
		monje2.start();
		monje3.start();
		monje4.start();
		monje5.start();

	}
}
