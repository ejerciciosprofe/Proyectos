package org.david.BaseDatos1;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Main {

	public static void main(String[] args) {

		boolean seguir = true;
		int seleccion;
		String tabla;
		Scanner kbInicial = new Scanner(System.in);
		Metodos metodo = new Metodos("tienda", "root", "Itep");

		while (seguir) {
			seleccion = 0;
			System.out.println("* * * * MENU TIENDA * * * *");
			System.out.println("");
			System.out.println("1 - Printear Datos de la base de datos");
			System.out.println("2 - Añadir Datos");
			System.out.println("3 - Datos por fabricante");
			System.out.println("4 - Los 5 mas baratos");
			System.out.println("5 - Editar precio");
			System.out.println("6 - Borrar archivo");
			System.out.println("7 - Salir");
			try {
				seleccion = kbInicial.nextInt();
			} catch (InputMismatchException IME) {
				System.out.println("Tienes que poner un numero");
			}
			switch (seleccion) {
			case 1:
				System.out.println("Dime de que tabla: ");
				Scanner kb1 = new Scanner(System.in);
				tabla = kb1.nextLine();
				metodo.mostrarDatos(tabla);
				break;
			case 2:
				Scanner kb2 = new Scanner(System.in);
				System.out.println("Dime de que tabla: ");
				tabla = kb2.nextLine();
				System.out.println("Dime el nombre del producto: ");
				String producto = kb2.nextLine();
				System.out.println("Dime el precio: ");
				double precio = kb2.nextDouble();
				System.out.println("Dime el id del fabricante: ");
				int fabricante = kb2.nextInt();
				metodo.insertarDatos(producto, precio, fabricante);
				break;
			case 3:
				Scanner kb3 = new Scanner(System.in);
				System.out.println("Dime el nombre del farbricante: ");
				String NombreFabricante = kb3.next();
				metodo.mostrarProductosFabricante(NombreFabricante);
				break;
			case 4:
				System.out.println("Los 5 mas baratos: ");
				metodo.mostrarMasBaratos();
				break;
			case 5:
				Scanner kb5 = new Scanner(System.in);
				System.out.println("Dime el id del producto: ");
				int id2 = kb5.nextInt();
				System.out.println("Dime el precio: ");
				double precio2 = kb5.nextDouble();
				metodo.editarPrecio(id2,precio2);
				break;
			case 6:
				Scanner kb6 = new Scanner(System.in);
				System.out.println("Dime el id del producto: ");
				int id3 = kb6.nextInt();
				metodo.borrarArticulo(id3);
				break;
			default:
				seguir = false;
				break;
			}

		}
	}
}
