package org.david.BaseDatos1;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
public class Metodos {
	
	PreparedStatement stmt;
	String query;
	Connection connection;
	Conexion conexion;
	private String url = "jdbc:mysql://localhost:3306/dam?characterEncoding=utf8";
	private String usuario = "root";
	private String contraseña = "Itep";
	
	public Metodos(String url, String usuario, String contraseña) {
		this.url = "jdbc:mysql://localhost:3306/"+url+"?characterEncoding=utf8";;
		this.usuario = usuario;
		this.contraseña = contraseña;
		establecerConexion(this.url,this.usuario,this.contraseña);
	}
	
	protected void establecerConexion(String url, String usuario, String contraseña) {
		conexion = new Conexion(url,usuario,contraseña);
		conexion.establecerConexion();
		connection = conexion.getConexion();
	}
	
	protected void insertarDatos(String producto, double precio, int fabricador) {
		try {
			String query = "insert into producto (nombre, precio, codigo_fabricante) values (?, ?, ?)";
			stmt = connection.prepareStatement(query);
			stmt.setString(1, producto);
			stmt.setDouble(2, precio);
			stmt.setInt(3, fabricador);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected void mostrarProductosFabricante(String fabricador) {
		String query = "select * from producto join fabricante on fabricante.codigo = producto.codigo_fabricante where fabricante.nombre = \""+fabricador+"\"";
		try {
			stmt = connection.prepareStatement(query);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()) {
				System.out.println("Id: "+rs.getInt(1)+"\n Producto: "+rs.getString(2)+"\n Precio: "+rs.getDouble(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected void mostrarMasBaratos() {
		String query = "select * from producto order by precio asc LIMIT 5";
		try {
			stmt = connection.prepareStatement(query);
			ResultSet rs = stmt.executeQuery(); 
			while (rs.next()) {
				System.out.println("Id: "+rs.getInt(1)+"\n Producto: "+rs.getString(2)+"\n Precio: "+rs.getDouble(3));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void editarPrecio(int codigo, double precio ) {
		String query = "update producto set producto.precio = "+precio+" where producto.codigo = "+codigo;
		try {
			stmt = connection.prepareStatement(query);
			int rs = stmt.executeUpdate(); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void borrarArticulo(int codigo) {
		String query = "delete from producto where producto.codigo = "+codigo;
		try {
			stmt = connection.prepareStatement(query);
			int rs = stmt.executeUpdate(); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void mostrarDatos(String tabla) {
		try {
			String query = "select * from "+tabla;
			stmt = connection.prepareStatement(query);
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				System.out.println("Id: "+rs.getInt(1)+"\n Producto: "+rs.getString(2)+"\n Precio: "+rs.getDouble(3)+"\n Codigo Fabricante: "+rs.getInt(4));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}