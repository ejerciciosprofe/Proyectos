package dao;

import java.util.List;
import modelos.Cliente;
import modelos.Empleado;
import modelos.Producto;

public interface ClienteDAO {
	
	public Cliente obtenerCliente(int id);
	
	public Cliente crearCliente(int id, String nombre, int cod_empleado, List<Producto> comida);
	
	public Cliente eliminarCliente(int id);
	
	public Cliente modificarCliente(int id, String nombre, int cod_empleado);
	
	public void ultimosClientes();
	
}
