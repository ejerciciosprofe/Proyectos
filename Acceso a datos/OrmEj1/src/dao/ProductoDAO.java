package dao;

import modelos.Producto;

public interface ProductoDAO {
	
	public Producto obtenerProducto(int id);
	
	public Producto crearProducto(int id, String nombre);
	
	public Producto eliminarProducto(int id);
	
	public Producto modificarProducto(int id, String nombre);
	
	public void comidaMasPedida();
	
}
