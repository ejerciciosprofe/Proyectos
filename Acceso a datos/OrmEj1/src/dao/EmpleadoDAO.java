package dao;

import modelos.Cliente;
import modelos.Empleado;

public interface EmpleadoDAO {
	
	public Empleado obtenerEmpleado(int id);
	
	public Empleado crearEmpleado(int id, String nombre);
	
	public Empleado eliminarEmpleado(int id);
	
	public Empleado modificarEmpleado(int id, String nombre);
	
	public void empleadoDelMes();
	
}
