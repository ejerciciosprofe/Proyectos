package Main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;

import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.internal.build.AllowSysOut;

import Utilidades.Utilidades;
import controlador.ClienteController;
import controlador.EmpleadoController;
import controlador.ProductoController;
import modelos.Cliente;
import modelos.Empleado;
import modelos.Producto;

public class Main {

	private static Scanner kb = new Scanner(System.in);

	static Session session;

	static ClienteController cc;
	static EmpleadoController ec;
	static ProductoController pc;

	public static void menu(String modi) {

		int key, id, idAux;
		String nombre;

		try {
			System.out.println("============ MENU ============ \n");
			System.out.println("      QUE QUIERES HACER \n");
			System.out.println("============================== \n");
			System.out.println("1 - Consultar " + modi);
			System.out.println("2 - Añadir " + modi);
			System.out.println("3 - Eliminar " + modi);
			System.out.println("4 - Modificar " + modi);
			if (modi.equalsIgnoreCase("cliente")) {
				System.out.println("5 - Pedir comida " + modi);
			} else if (modi.equalsIgnoreCase("empleado")) {
				System.out.println("5 - Empleado Top " + modi);
			} else {
				System.out.println("5 - Comida estrella " + modi);
			}
			if (modi.equalsIgnoreCase("cliente")) {
				System.out.println("6 - Ultimos 5 clientes " + modi);
			}
			key = kb.nextInt();
			switch (key) {
			case 1:
				if (modi.equalsIgnoreCase("cliente")) {
					listaCliente();
					System.out.println("¿Que id quieres revisar?");
					id = kb.nextInt();
					System.out.println(cc.obtenerCliente(id));
				} else if (modi.equalsIgnoreCase("empleado")) {
					listaEmpleado();
					System.out.println("¿Que id quieres revisar?");
					id = kb.nextInt();
					System.out.println(ec.obtenerEmpleado(id));
				} else {
					listaComida();
					System.out.println("¿Que id quieres revisar?");
					id = kb.nextInt();
					System.out.println(pc.obtenerProducto(id));
				}
				break;
			case 2:
				if (modi.equalsIgnoreCase("cliente")) {
					int id_producto_pedido;
					System.out.println("¿Cual es tu nombre?");
					nombre = kb.next();
					System.out.println("¿Quien te ha atendido (su id)?");
					idAux = kb.nextInt();
					List<Producto> listaProducto = new ArrayList<Producto>();
					// while(listaProducto.isEmpty()) {
					while (true) {
						try {
							Scanner kb1 = new Scanner(System.in);
							listaComida();
							System.out.println("¿Que quieres pedir (pon la id) o pon \"n\" si has terminado?");
							id_producto_pedido = kb1.nextInt();
							listaProducto.add(pc.obtenerProducto(id_producto_pedido));
						} catch (Exception e) {
							if (!listaProducto.isEmpty()) {
								break;
							}
						}
					}
					// }
					cc.crearCliente(1, nombre, idAux, listaProducto);
				} else if (modi.equalsIgnoreCase("empleado")) {
					System.out.println("¿Como se llama el nuevo empleado?");
					nombre = kb.next();
					ec.crearEmpleado(1, nombre);
				} else {
					System.out.println("¿Cual es el nuevo plato?");
					nombre = kb.next();
					pc.crearProducto(1, nombre);
				}
				break;
			case 3:
				if (modi.equalsIgnoreCase("cliente")) {
					listaCliente();
					System.out.println("¿Cual es la id que quieres borrar?");
					id = kb.nextInt();
					cc.eliminarCliente(id);
				} else if (modi.equalsIgnoreCase("empleado")) {
					listaEmpleado();
					System.out.println("¿Cual es la id que quieres borrar?");
					id = kb.nextInt();
					ec.eliminarEmpleado(id);
				} else {
					listaComida();
					System.out.println("¿Cual es la id que quieres borrar?");
					id = kb.nextInt();
					pc.eliminarProducto(id);
				}
				break;
			case 4:
				System.out.println("¿Cual es tu id?");
				id = kb.nextInt();
				System.out.println("¿Cual es el nuevo nombre?");
				nombre = kb.next();
				if (modi.equalsIgnoreCase("cliente")) {
					System.out.println("¿Quien te ha atendido (su id)?");
					idAux = kb.nextInt();
					cc.modificarCliente(id, nombre, idAux);
				} else if (modi.equalsIgnoreCase("empleado")) {
					ec.modificarEmpleado(id, nombre);
				} else {
					pc.modificarProducto(id, nombre);
				}
				break;
			case 5:
				if (modi.equalsIgnoreCase("cliente")) {
					listaCliente();
					System.out.println("¿Cual es tu id?");
					id = kb.nextInt();
					int cont = 1, id_producto_pedido;
					listaComida();
					System.out.println("¿Que quieres pedir (pon la id)?");
					id_producto_pedido = kb.nextInt();
					List<Producto> aux = cc.obtenerCliente(id).getProducto();
					aux.add(pc.obtenerProducto(id_producto_pedido));
					cc.obtenerCliente(id).setProducto(aux);
				} else if (modi.equalsIgnoreCase("empleado")) {
					ec.empleadoDelMes();
				} else {
					pc.comidaMasPedida();
				}
				break;
			case 6:
				if (modi.equalsIgnoreCase("cliente")) {
					cc.ultimosClientes();
				}
				break;
			default:
				break;
			}
		} catch (ObjectNotFoundException a) {
			System.err.println(modi + " no encontrado");
		}

	}

	protected static void menuListas() {
		int key;
		System.out.println("=========== LISTAS =========== \n");
		System.out.println("  A QUE LISTA QUIERES ACCEDER \n");
		System.out.println("============================== \n");
		System.out.println("1 - Cliente");
		System.out.println("2 - Empleado");
		System.out.println("3 - Comida");
		System.out.println("4 - Todas");
		System.out.println("5 - Salir \n");
		key = kb.nextInt();
		switch (key) {
		case 1:
			System.out.println("========== CLIENTES ========== \n");
			listaCliente();
			break;
		case 2:
			System.out.println("========== EMPLEADO ========== \n");
			listaEmpleado();
			break;
		case 3:
			System.out.println("========== PRODUCTO ========== \n");
			listaComida();
			break;
		case 4:
			System.out.println("========== CLIENTES ========== \n");
			listaCliente();
			System.out.println("========== PRODUCTO ========== \n");
			listaComida();
			System.out.println("========== EMPLEADO ========== \n");
			listaEmpleado();
			break;
		default:
			break;
		}
	}

	public static void listaComida() {
		int cont = 1;
		while (true) {
			try {
				System.out.println(pc.obtenerProducto(cont) + "\n");
				cont++;
			} catch (Exception e) {
				break;
			}
		}
	}

	public static void listaEmpleado() {
		int cont = 1;
		while (true) {
			try {
				System.out.println(ec.obtenerEmpleado(cont) + "\n");
				cont++;
			} catch (Exception e) {
				break;
			}
		}
	}

	public static void listaCliente() {
		int cont = 1;
		while (true) {
			try {
				System.out.println(cc.obtenerCliente(cont) + "\n");
				cont++;
			} catch (Exception e) {
				break;
			}
		}
	}

	public static void main(String[] args) {
		try {

			session = Utilidades.getSessionFactory().openSession();
			cc = new ClienteController(session);
			ec = new EmpleadoController(session);
			pc = new ProductoController(session);

			int key;
			boolean bandera = true;
			try {
				while (bandera) {
					System.out.println("============ MENU ============ \n");
					System.out.println("  A QUE BASE QUIERES ACCEDER \n");
					System.out.println("============================== \n");
					System.out.println("1 - Cliente");
					System.out.println("2 - Empleado");
					System.out.println("3 - Comida");
					System.out.println("4 - Listas");
					System.out.println("5 - Salir \n");
					key = kb.nextInt();
					switch (key) {
					case 1:
						menu("Cliente");
						break;
					case 2:
						menu("Empleado");
						break;
					case 3:
						menu("Comida");
						break;
					case 4:
						menuListas();
						break;
					default:
						bandera = false;
						break;
					}
				}
			} catch (Exception e) {
				System.err.println("Error");
			}

			session.close();
		} catch (Exception y) {
			System.err.println("Un error a ocurrido");
		}
	}

}
