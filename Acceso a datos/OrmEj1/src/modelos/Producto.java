package modelos;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "producto")
public class Producto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cod_comida")
	private int cod_comida;

	@Column(name = "nom_comida")
	private String nom_comida;
	
	@ManyToMany(mappedBy = "producto")
	private List<Cliente> cliente;
	
	public Producto() {
	}
	
	@Override
	public String toString() {
		return "Comida {\n cod_comida = " + cod_comida + ", \n nom_comida = \"" + nom_comida + "\"\n}";
	}

	public int getCod_comida() {
		return cod_comida;
	}

	public void setCod_comida(int cod_comida) {
		this.cod_comida = cod_comida;
	}

	public String getNom_comida() {
		return nom_comida;
	}

	public void setNom_comida(String nom_comida) {
		this.nom_comida = nom_comida;
	}

	public List<Cliente> getCliente() {
		return cliente;
	}

	public void setCliente(List<Cliente> cliente) {
		this.cliente = cliente;
	}
	
	
}
