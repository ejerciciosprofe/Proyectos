package modelos;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "empleado")
public class Empleado {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cod_empleado")
	private int cod_empleado;
	
	private String nom_empleado;

	@OneToMany(mappedBy = "empleado")
	private List<Cliente> cliente;

	public Empleado() {
	}

	@Override
	public String toString() {
		return "Empleado { \n cod_empleado = " + cod_empleado + ", \n nom_empleado = \"" + nom_empleado + "\"\n}";
	}

	public Empleado(int cod_empleado, String nom_empleado) {
		super();
		this.cod_empleado = cod_empleado;
		this.nom_empleado = nom_empleado;
	}
	
	public int getCod_empleado() {
		return cod_empleado;
	}

	public void setCod_empleado(int cod_empleado) {
		this.cod_empleado = cod_empleado;
	}

	public String getNom_empleado() {
		return nom_empleado;
	}

	public void setNom_empleado(String nom_empleado) {
		this.nom_empleado = nom_empleado;
	}

	public List<Cliente> getCliente() {
		return cliente;
	}

	public void setCliente(List<Cliente> cliente) {
		this.cliente = cliente;
	}

}
