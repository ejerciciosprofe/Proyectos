package modelos;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cliente")
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cod_cliente")
	private int cod_cliente;

	@Column(name = "nom_cliente")
	private String nom_cliente;
	
	@OneToOne()
	@JoinColumn(name = "cod_empleado")
	private Empleado empleado;

	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="producto_cliente",
	joinColumns = {@JoinColumn(name="cod_cliente")},
	inverseJoinColumns = {@JoinColumn(name="cod_comida")})
	private List<Producto> producto;
	
	public Cliente() {
	}
	
	@Override
	public String toString() {
		return "Cliente { \n cod_cliente = " + cod_cliente + ", \n nom_cliente = \"" + nom_cliente + "\", \n cod_empleado = " + empleado.getCod_empleado() +"\n}";
	}

	public int getCod_cliente() {
		return cod_cliente;
	}

	public void setCod_cliente(int cod_cliente) {
		this.cod_cliente = cod_cliente;
	}

	public String getNom_cliente() {
		return nom_cliente;
	}

	public void setNom_cliente(String nom_cliente) {
		this.nom_cliente = nom_cliente;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public List<Producto> getProducto() {
		return producto;
	}

	public void setProducto(List<Producto> producto) {
		this.producto = producto;
	}
}
