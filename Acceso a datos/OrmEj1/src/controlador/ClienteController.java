package controlador;

import java.util.Collections;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import Utilidades.Utilidades;
import dao.ClienteDAO;
import modelos.Cliente;
import modelos.Empleado;
import modelos.Producto;

public class ClienteController implements ClienteDAO{
	
	Session session = null;

	public ClienteController(Session session) {
		super();
		this.session = session;
	}
	
	
	
	@Override
	public Cliente obtenerCliente(int id) {
		Cliente cliente = null;

		try {
			Transaction transaction = null;
			transaction = session.beginTransaction();

			cliente = (Cliente) session.load(Cliente.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Cliente no encontrado");
		}

		return cliente;
	}



	@Override
	public Cliente crearCliente(int id, String nombre, int cod_empleado, List<Producto> comida) {
		Cliente cliente = null;

		try {
			cliente = new Cliente();
			cliente.setCod_cliente(id);
			cliente.setNom_cliente(nombre);
			EmpleadoController ec = new EmpleadoController(session);
			cliente.setEmpleado(ec.obtenerEmpleado(cod_empleado));
			cliente.setProducto(comida);
			session.beginTransaction();
			session.save(cliente);
			session.getTransaction().commit();			
			Transaction transaction = null;
			transaction = session.beginTransaction();

			cliente = (Cliente) session.load(Cliente.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Cliente no encontrado");
		}
		
		try {
		}catch(NullPointerException e) {
			System.err.println("No existe el empleado");
		}
		return cliente;
	}



	@Override
	public Cliente eliminarCliente(int id) {
		Cliente cliente = null;

		try {
			cliente = obtenerCliente(id); 
			session.beginTransaction();
			session.delete(cliente);
			session.getTransaction().commit();
			
			Transaction transaction = null;
			transaction = session.beginTransaction();

			cliente = (Cliente) session.load(Cliente.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Cliente no encontrado");
		}
		
		try {
		}catch(NullPointerException e) {
			System.err.println("No existe el empleado");
		}
		return cliente;
	}



	@Override
	public Cliente modificarCliente(int id, String nombre, int cod_empleado) {
		Cliente cliente = null;

		try {
			cliente = obtenerCliente(id);
			cliente.setNom_cliente(nombre);
			EmpleadoController ec = new EmpleadoController(session);
			cliente.setEmpleado(ec.obtenerEmpleado(cod_empleado));
			session.beginTransaction();
			session.save(cliente);
			session.getTransaction().commit();
			
			Transaction transaction = null;
			transaction = session.beginTransaction();

			cliente = (Cliente) session.load(Cliente.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Cliente no encontrado");
		}
		
		try {
			System.out.println(cliente.toString());
		}catch(NullPointerException e) {
			System.err.println("No existe el empleado");
		}
		return cliente;
	}



	@Override
	public void ultimosClientes() {
		
		List<Object[]> employees = session.createNativeQuery("Select * from cliente order by cod_cliente DESC limit 5").list();
		
		
		for(Object[] i: employees) {
			int clientenum = (int) i[0];
			System.out.println(obtenerCliente(clientenum));
		}
	}
	
}
