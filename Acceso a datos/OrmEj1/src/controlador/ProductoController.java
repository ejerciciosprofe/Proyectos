package controlador;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.ProductoDAO;
import modelos.Cliente;
import modelos.Empleado;
import modelos.Producto;

public class ProductoController implements ProductoDAO{
	
	Session session = null;

	public ProductoController(Session session) {
		super();
		this.session = session;
	}
	
	@Override
	public Producto obtenerProducto(int id) {
		Producto producto = null;

		try {
			Transaction transaction = null;
			transaction = session.beginTransaction();

			producto = (Producto) session.load(Producto.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Cliente no encontrado");
		}

		return producto;
	}

	@Override
	public Producto crearProducto(int id, String nombre) {
		Producto producto = null;

		try {
			producto = new Producto();
			producto.setCod_comida(id);
			producto.setNom_comida(nombre);
			session.beginTransaction();
			session.save(producto);
			session.getTransaction().commit();
			
			Transaction transaction = null;
			transaction = session.beginTransaction();

			producto = (Producto) session.load(Producto.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Cliente no encontrado");
		}

		return producto;
	}

	@Override
	public Producto eliminarProducto(int id) {
		Producto producto = null;

		try {
			producto = obtenerProducto(id); 
			session.beginTransaction();
			session.delete(producto);
			session.getTransaction().commit();
			
			Transaction transaction = null;
			transaction = session.beginTransaction();

			producto = (Producto) session.load(Producto.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Cliente no encontrado");
		}
		
		try {
		}catch(NullPointerException e) {
			System.err.println("No existe el empleado");
		}
		return producto;
	}

	@Override
	public Producto modificarProducto(int id, String nombre) {
		Producto producto = null;

		try {
			producto = obtenerProducto(id); 
			producto.setNom_comida(nombre);
			session.beginTransaction();
			session.save(producto);
			session.getTransaction().commit();
			
			Transaction transaction = null;
			transaction = session.beginTransaction();

			producto = (Producto) session.load(Producto.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Cliente no encontrado");
		}
		
		try {
		}catch(NullPointerException e) {
			System.err.println("No existe el empleado");
		}
		return producto;
	}

	@Override
	public void comidaMasPedida() {
		
		List<Object[]> employees = session.createNativeQuery("SELECT COUNT(producto_cliente.cod_comida), cod_comida\r\n"
				+ "FROM producto_cliente\r\n"
				+ "group by cod_comida\r\n"
				+ "order by COUNT(cod_comida) DESC").list();
		
		Object[] mes = employees.get(0);
		int empleadoMes = (int) mes[1];
		
		System.out.println("La comida mas pedida es: "+obtenerProducto(empleadoMes).getNom_comida());
		
		/*
		List<Cliente> employees = session.createQuery("from Cliente").list();

		ArrayList<Integer> ana = new ArrayList<Integer>();

		for(Cliente i: employees) {
			for(Producto j: i.getProducto()) {
				ana.add(j.getCod_comida());
			}
		}
		
		ArrayList<Integer> ana2 = new ArrayList<Integer>();

		HashMap<Integer, Integer> map = new HashMap<>();
		for (int i : ana) {
		   if (map.containsKey(i)) {
		       map.put(i, map.get(i) + 1);
		   } else {
		       map.put(i, 1);
		   }
		}


		int maxRepeated = 0;
		int repeatedElement = 0;
		for (Entry<Integer, Integer> entry : map.entrySet()) {
		   if (entry.getValue() > maxRepeated) {
		       maxRepeated = entry.getValue();
		       repeatedElement = entry.getKey();
		   }
		}

		System.out.println("La comida mas pedida es: \n \n" +obtenerProducto(repeatedElement).toString()+"\n");

		for (Entry<Integer, Integer> entry : map.entrySet()) {
		   if (entry.getValue() == maxRepeated && entry.getKey() != repeatedElement) {
		       System.out.println(obtenerProducto(entry.getKey()).toString()+"\n");
		   }
		}*/
		
	}

}
