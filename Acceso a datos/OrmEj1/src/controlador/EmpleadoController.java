package controlador;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.EmpleadoDAO;
import modelos.Cliente;
import modelos.Empleado;

public class EmpleadoController implements EmpleadoDAO{

	Session session = null;

	public EmpleadoController(Session session) {
		super();
		this.session = session;
	}
	
	@Override
	public Empleado obtenerEmpleado(int id) {
		Empleado empleado = null;

		try {
			Transaction transaction = null;
			transaction = session.beginTransaction();

			empleado = (Empleado) session.load(Empleado.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Cliente no encontrado");
		}

		return empleado;
	}

	@Override
	public Empleado crearEmpleado(int id, String nombre) {
		Empleado empleado = null;

		try {
			empleado = new Empleado();
			empleado.setCod_empleado(id);
			empleado.setNom_empleado(nombre);
			session.beginTransaction();
			session.save(empleado);
			session.getTransaction().commit();
			
			Transaction transaction = null;
			transaction = session.beginTransaction();

			empleado = (Empleado) session.load(Empleado.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Cliente no encontrado");
		}
		
		try {
		}catch(NullPointerException e) {
			System.err.println("Error");
		}
		return empleado;
	}
	
	@Override
	public Empleado eliminarEmpleado(int id) {
		Empleado empleado = null;

		try {
			empleado = obtenerEmpleado(id); 
			session.beginTransaction();
			session.delete(empleado);
			session.getTransaction().commit();
			
			Transaction transaction = null;
			transaction = session.beginTransaction();

			empleado = (Empleado) session.load(Empleado.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Cliente no encontrado");
		}
		
		try {
		}catch(NullPointerException e) {
			System.err.println("No existe el empleado");
		}
		return empleado;
	}



	@Override
	public Empleado modificarEmpleado(int id, String nombre) {
		Empleado empleado = null;

		try {
			empleado = obtenerEmpleado(id); 
			empleado.setNom_empleado(nombre);
			session.beginTransaction();
			session.save(empleado);
			session.getTransaction().commit();
			
			Transaction transaction = null;
			transaction = session.beginTransaction();

			empleado = (Empleado) session.load(Empleado.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Cliente no encontrado");
		}
		
		try {
		}catch(NullPointerException e) {
			System.err.println("No existe el empleado");
		}
		return empleado;
	}

	@Override
	public void empleadoDelMes() {
		
		List<Object[]> employees = session.createNativeQuery("SELECT COUNT(cod_empleado), cod_empleado\r\n"
				+ "FROM cliente\r\n"
				+ "group by cod_empleado\r\n"
				+ "order by COUNT(cod_empleado) DESC").list();
		
		Object[] mes = employees.get(0);
		int empleadoMes = (int) mes[1];
		
		System.out.println("El empleado del mes es: "+obtenerEmpleado(empleadoMes).getNom_empleado());
		
		/*
		ArrayList<Integer> ana = new ArrayList<Integer>();

		for(Cliente i: employees) {
			ana.add(i.getEmpleado().getCod_empleado());
		}

		HashMap<Integer, Integer> map = new HashMap<>();
		for (int i : ana) {
		   if (map.containsKey(i)) {
		       map.put(i, map.get(i) + 1);
		   } else {
		       map.put(i, 1);
		   }
		}

		int maxRepeated = 0;
		int repeatedElement = 0;
		for (Entry<Integer, Integer> entry : map.entrySet()) {
		   if (entry.getValue() > maxRepeated) {
		       maxRepeated = entry.getValue();
		       repeatedElement = entry.getKey();
		   }
		}

		System.out.println("El empleado del mes: \n \n" +obtenerEmpleado(repeatedElement).toString()+"\n");

		for (Entry<Integer, Integer> entry : map.entrySet()) {
		   if (entry.getValue() == maxRepeated && entry.getKey() != repeatedElement) {
		       System.out.println(obtenerEmpleado(entry.getKey()).toString()+"\n");
		   }
		}
		*/
	}

}
