package ejercicioXML2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Main {

	public static void main(String[] args) {

		boolean seguir = true;
		int seleccion;
		Scanner kb = new Scanner(System.in);
		//Path path = Path.of("src/ejercicioXML2/juegos.xml");
        File xml = new File("src/ejercicioXML2/juegos.xml");
        try {
			xml.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        DocumentBuilder builder = Metodos.createBuilder();
		Document document = null;
        try {
            document = builder.parse(xml);
        } catch (IOException | SAXException ex) {
            System.err.println("Error al crear el Document");
            System.err.println(ex.getMessage());
            System.exit(-2);
        }
		while (seguir) {
			seleccion = 0;
			System.out.println("* * * * MENU XML * * * *");
			System.out.println("");
			System.out.println("1 - Printear Datos");
			System.out.println("2 - A�adir Datos");
			System.out.println("3 - Eliminar Datos");
			System.out.println("4 - Salir");
			try {
				seleccion = kb.nextInt();
			}catch(InputMismatchException IME) {
				System.out.println("Tienes que poner un numero");
			}
			switch (seleccion) {
			case 1:
				Metodos.leerXML();
				break;
			case 2:
				try {
					Metodos.escribirXML();
				} catch (ParserConfigurationException | SAXException | IOException e) {
					e.printStackTrace();
				}
				break;
			case 3:
				NodeList lista = document.getElementsByTagName("listadejuegos").item(0).getChildNodes();
				try {
					Metodos.eliminarElementos(lista);
				} catch (SAXException | IOException e) {
					e.printStackTrace();
				}
				break;
			default:
				seguir = false;
				break;
			}
		}
	}
}