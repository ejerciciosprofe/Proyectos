package ejercicioXML2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.util.Scanner;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class Metodos {
	
	static Scanner kb = new Scanner(System.in);
	
	public Metodos() {
		
	}
	
	public static void leerXML() {
        Path path = Path.of("src/ejercicioXML2/juegos.xml");
        File xml = path.toFile();
        DocumentBuilder builder = createBuilder();
        Document document = null;
        try {
            document = builder.parse(xml);
        } catch (IOException | SAXException ex) {
            System.err.println("Error al crear el Document");
            System.err.println(ex.getMessage());
            System.exit(-2);
        }
        NodeList listaInicial = document.getElementsByTagName("listadejuegos").item(0).getChildNodes();
        listarElementos(listaInicial);
    }

	public static DocumentBuilder createBuilder(){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            System.err.println("Error al crear el DocumentBuilder");
            System.err.println(ex.getMessage());
            System.exit(-1);
        }
        return builder;
    }
	
	public static void listarElementos(NodeList list) {
        for (int i = 0; i < list.getLength(); i++) {
            Node node = list.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
            	Element elemento = (Element) node;

    				System.out.println("Titulo: " + getNodo("titulo", elemento));
    				System.out.println("Genero: " + getNodo("genero", elemento));
    				System.out.println("Plataforma: " + getNodo("plataforma", elemento));
    				System.out.println("Fecha de lanzamiento: " + getNodo("fechadelanzamiento", elemento));
    				
                 
            }
        }
    }
	
	public static String  getNodo(String etiqueta, Element elem) {
		NodeList nodo = elem.getElementsByTagName(etiqueta).item(0).getChildNodes();
		Node valornodo = (Node) nodo.item(0);
		return valornodo.getNodeValue();
	}

	public static void escribirXML() throws ParserConfigurationException, SAXException, IOException {
		
		String tituloJuego, generoJuego, plataformaJuego, fechadelanzamientoJuego;
		
		System.out.println("Dime el titulo: ");
		tituloJuego = kb.nextLine();
		System.out.println("Dime el genero: ");
		generoJuego = kb.nextLine();
		System.out.println("Dime la plataforma: ");
		plataformaJuego = kb.nextLine();
		System.out.println("Dime la fecha de lanzamiento: ");
		fechadelanzamientoJuego = kb.nextLine();
		
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder newDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = newDocumentBuilder.parse("src/ejercicioXML2/juegos.xml");
        
        document.getDocumentElement().normalize();
		
		Element root = document.getDocumentElement();
		
        Element juego = document.createElement("juego");

        Element titulo = document.createElement("titulo");
        titulo.setTextContent(tituloJuego);
        Element genero = document.createElement("genero");
        genero.setTextContent(generoJuego);
        Element plataforma = document.createElement("plataforma");
        plataforma.setTextContent(plataformaJuego);
        Element fechadelanzamiento = document.createElement("fechadelanzamiento");
        fechadelanzamiento.setTextContent(fechadelanzamientoJuego);
        juego.appendChild(titulo);
        juego.appendChild(genero);
        juego.appendChild(plataforma);
        juego.appendChild(fechadelanzamiento);

        root.appendChild(juego);
        
        Source origen = new DOMSource(document);
        Result result = new StreamResult(new File("src/ejercicioXML2/juegos.xml"));
        Transformer transformer = null;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
        } catch (TransformerConfigurationException ex) {
            System.err.println("Error al crear el Transfomer");
            System.out.println(ex.getMessage());
            System.exit(-3);
        }
        try {
            transformer.transform(origen, result); // transforma los datos: de la RAM al fichero
        } catch (TransformerException ex) {
            System.err.println("Error al transformar el origen en el destino");
            System.out.println(ex.getMessage());
            System.exit(-3);
        }
	}
	
	public static void eliminarElementos(NodeList list) throws SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder newDocumentBuilder = null;
		try {
			newDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Document document = newDocumentBuilder.parse("src/ejercicioXML2/juegos.xml");
		System.out.println("Introduce el t�tulo del juego que quieres borrar");
		String elementoBorrar = kb.nextLine();

		for (int i = 0; i < list.getLength(); i++) {
			Node node = list.item(i);
			if ((node.getNodeType() == Node.ELEMENT_NODE)) {
				Element elemento =  (Element) node;
				System.out.println(getNodo("titulo", elemento));
				if (getNodo("titulo", elemento).equalsIgnoreCase(elementoBorrar)) {
					elemento.getParentNode().removeChild(elemento);
					}
				}
			}
		Source origen = new DOMSource(document);
        Result result = new StreamResult(new File("src/ejercicioXML2/juegos.xml"));
        Transformer transformer = null;
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
		} catch (TransformerConfigurationException | TransformerFactoryConfigurationError e) {
			System.out.println("Error al crear el transformer");
			e.printStackTrace();
		}
        try {
			transformer.transform(origen, result);
		} catch (TransformerException e) {
			System.err.println("Error al transformar el origen en el destino");
			e.printStackTrace();
		}
	}

	
	public static void cargarDatos(Document doc) {
		Source origen = new DOMSource(doc);
        Result result = new StreamResult(new File("src/ejercicioXML2/juegos.xml"));
        Transformer transformer = null;
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
		} catch (TransformerConfigurationException | TransformerFactoryConfigurationError e) {
			System.out.println("Error al crear el transformer");
			e.printStackTrace();
		}
        try {
			transformer.transform(origen, result);
		} catch (TransformerException e) {
			System.err.println("Error al transformar el origen en el destino");
			e.printStackTrace();
		}
	}

	
	
}
