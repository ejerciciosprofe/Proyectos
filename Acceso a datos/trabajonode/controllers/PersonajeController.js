const Personaje = require('../models/Personaje');

exports.verPersonajes = (req,res)=>{
    Personaje.find().then(personaje => {
        res.json({personaje});
    })
}

exports.favorito = (req, res) => {
    const id = 9;
    Personaje.findOne({ id: id })
        .then(personaje => {
            if (!personaje) {
                return res.status(404).json({ msg: 'Personaje no encontrado' });
            }
            res.json({personaje});
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({ msg: 'Error interno del servidor' });
        });
};

exports.eliminarPersonaje = (req, res) => {
    const id = req.params.id;
    Personaje.remove({ id: id })
        .then(() => {
            console.log({ msg: 'Personaje eliminado' });
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({ msg: 'Error interno del servidor' });
        });
}

exports.crearPersonaje = (req, res) => {
    const nuevoPersonaje = new Personaje({
        id: 12,
        name: "Albert Einstein",
        species: "Human",
        image: "https://rickandmortyapi.com/api/character/avatar/11.jpeg"
    });
    nuevoPersonaje.save()
        .then(personaje => {
            res.json(personaje);
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({ msg: 'Error interno del servidor' });
        });
}