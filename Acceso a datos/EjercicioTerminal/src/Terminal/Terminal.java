package Terminal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Pattern;

// Falta el writte (meterlo en el main)
// Errores

public class Terminal {

	protected static Scanner kb = new Scanner(System.in);
	protected static String globalPath = System.getProperty("user.dir");

	protected static String nameUsuario() {
		String name = System.getProperty("user.name");
		return name;
	}

	protected static String nameComputer() {
		String SystemName = "";
		try {
			SystemName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return SystemName;
	}

	protected static String pwd() {
		return globalPath;
	}

	String returnearPath() {
		return this.getClass().getClassLoader().getResource("").getPath();
	}

	protected void mkFile(String[] directory) {
		try {
			File myObj = new File(globalPath, directory[1]);
			if (myObj.createNewFile()) {
				escribir(directory);
			} else {
				System.out.println("File already exists.");
			}
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

	protected static void cd(String[] directory) {
		if (directory.length < 2)
			System.out.println(pwd());
		else if (directory.length > 2) {
			File f = new File(globalPath, directory[1]);
			if (f.exists() && f.isDirectory())
				globalPath = f.getAbsolutePath();
			else
				System.out.println("El sistema no puede encontrar la ruta especificada.");
		} else {
			if (directory[1].equals("..")) {
				File f = new File(globalPath);
				if (f.getParent() != null)
					globalPath = f.getParent();
			} else if (directory[1].contains(":")) {
				File f = new File(directory[1]);
				if (f.exists() && f.isDirectory())
					globalPath = f.getAbsolutePath();
			} else {
				File f = new File(globalPath, directory[1]);
				if (f.exists() && f.isDirectory()) {
					globalPath = f.getAbsolutePath();
				} else {
					System.out.println("El sistema no puede encontrar la ruta especificada.");
				}
			}
		}
	}

	protected static void mkdir(String[] directory) {
		File f = new File(globalPath, directory[1]);
		if (f.mkdir()) {
			f.mkdirs();
		} else {
			System.out.println("El directorio ya existe o no puede ser creado");
		}
	}

	protected static void dir() {
		String[] pathnames;
		File f = new File(globalPath);
		pathnames = f.list();
		for (String pathname : pathnames)
			System.out.println(pathname);
	}

	protected static void info(String[] directory) {
		File file = new File(globalPath, directory[1]);
		int totalFiles = 0;
		if (file.exists() && file.isDirectory()) {
			System.out.println("Nombre file: " + file.getName());
			System.out.println("Ruta : " + file.getAbsolutePath());
			System.out.println("Tama�o : " + file.getTotalSpace() + " Bytes");
			String[] pathnames;
			File f = new File(globalPath, directory[1]);
			pathnames = f.list();
			for (String pathname : pathnames) {
				totalFiles++;
			}
			System.out.println("Numero de Archivos: " + totalFiles);
		} else {
			System.out.println("El sistema no puede encontrar la ruta especificada.");
		}
	}

	protected static void cat(String[] directory) {
		try {
			File file = new File(globalPath, directory[1]);
			if (!file.isFile())
				System.out.println("No se ha encontrado el file que quieres leer");
			FileReader reader = new FileReader(file);
			int data = reader.read();
			while (data != -1) {
				System.out.print((char) data);
				data = reader.read();
			}
			reader.close();
			System.out.println("");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected static void readpoint(String[] directory) throws FileNotFoundException {
		File text = new File(globalPath, directory[1]);
		try {
			Scanner scnr = new Scanner(text);
			int lineNumber = Integer.parseInt(directory[2]);
			while (scnr.hasNextLine()) {
				String line = scnr.nextLine();
				System.out.println(line);
				lineNumber++;
			}
		} catch (NoSuchElementException e) {
			System.out.println("No hay mas lineas en el documento");
		} catch (NumberFormatException e) {
			System.out.println("El tercer valor ha de ser un n�mero");
		} catch (FileNotFoundException e) {
			System.out.println("El file no ha sido encontrado");
		}
	}

	protected static void top(String[] directory) throws FileNotFoundException {
		File text = new File(globalPath, directory[1]);
		if (text.exists()) {
			Scanner scnr = new Scanner(text);
			int lineNumber = 0;
			try {
				while (lineNumber != Integer.parseInt(directory[2])) {
					String line = scnr.nextLine();
					System.out.println(line);
					lineNumber++;
				}
			} catch (NoSuchElementException e) {
				System.out.println("No hay mas lineas en el documento");
			} catch (NumberFormatException e) {
				System.out.println("El tercer valor ha de ser un n�mero");
			}
		}
		else {
			System.out.println("El file no ha sido encontrado");
		}
	}

	protected static void delete(String[] directory) {
		File fichero = new File(globalPath, directory[1]);
		if (fichero.delete()) {
		} else {
			System.out.println("El fichero no puede ser borrado");
		}
	}
	
	private void escribir(String[] directory) {
		Writer output = null;
		try {
			output = new BufferedWriter(new FileWriter(globalPath + "\\" + directory[1], true));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String texto = "";
		for (int i = 2; i < directory.length; i++)
			texto += directory[i] + " ";
		try {
			output.append(texto);
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void write(String[] directory) {
		if (directory.length < 2)
			System.out.println("La sintaxis del comando no es correcta");
		else {
			File file = new File(globalPath, directory[1]);
			if (file.exists()) { 
				escribir(directory);
			} else
				System.out.println("El archivo no existe");
		}
	}

	public static void help() {
		System.out.println("");
		System.out.println("HELP                		      -> Lista todos los comandos");
		System.out.println("CD                 		      -> Muestra la ruta del directorio actual");
		System.out.println("CD ..               		      ->  Se mueve al directorio padre");
		System.out.println("CD + NOMBRE         		      -> Lista archivos de ese directorio");
		System.out.println("MKDIR               		      -> Crea un directorio de la ruta actual");
		System.out.println("INFO <nombre>       		      -> Muestra la informaci�n del elemento indicado");
		System.out.println("CAT <nombreFichero> 		      -> Muestra el contenido de un fichero");
		System.out.println("TOP <numeroLineas><NombreFichero>     -> Muestra las l�neas especificadas de un fichero.");
		System.out.println("MKFILE <nombreFichero> <texto>        -> Crea un fichero con ese nombre y el contenido de texto");
		System.out.println("WRITE <nombreFichero> <texto>         -> A�ade 'texto' al final del fichero especificado");
		System.out.println("DIR 				      -> Lista los archivos o directorios de la ruta actual");
		System.out.println("READPOINT <nombreFichero1> <posici�n> -> Lee un archivo desde una determinada posici�n del puntero");
		System.out.println("DELETE <nombre>			      -> Borra el fichero, si es un directorio borra todo su contenido y a si mismo");
		System.out.println("CLOSE				      -> Cierra el programa");
		System.out.println("CLEAR 				      -> Vac�a la lista");
	}

}
