package Terminal;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
	
	protected static Scanner kb = new Scanner(System.in);
	
	public static void main(String[] args) {

		String comando = "";
		
		Terminal terminal = new Terminal();

		while (!comando.equalsIgnoreCase("exit")) {
			System.out.print(terminal.nameUsuario() + "@" + terminal.nameComputer() + "$> ");
			comando = kb.nextLine();
			String[] comandoArray = comando.split(" ");
			try {
				switch (comandoArray[0]) {
				case "help":
					terminal.help();
					break;
				case "exit":
					break;
				case "pwd":
					System.out.println(terminal.pwd());
					break;
				case "cd":
					terminal.cd(comandoArray);
					break;
				case "dir":
					terminal.dir();
					break;
				case "mkdir":
					terminal.mkdir(comandoArray);
					break;
				case "info":
					terminal.info(comandoArray);
					break;
				case "cat":
					terminal.cat(comandoArray);
					break;
				case "mkfile":
					terminal.mkFile(comandoArray);
					break;
				case "readpoint":
					try {
						terminal.readpoint(comandoArray);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
					break;
				case "top":
					try {
						terminal.top(comandoArray);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
					break;
				case "delete":
					terminal.delete(comandoArray);
					break;
				case "write":
					terminal.write(comandoArray);
					break;
				default:
					System.out.println("El comando no existe");
				}
			}catch(ArrayIndexOutOfBoundsException a) {
				System.out.println("El comando necesita otro parametro");
			}
		}

	}

}
