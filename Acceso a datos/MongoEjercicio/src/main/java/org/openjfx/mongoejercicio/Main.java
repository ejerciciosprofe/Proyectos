package org.openjfx.mongoejercicio;

import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.InsertOneResult;
import org.bson.Document;
import org.bson.types.ObjectId;

public class Main {

    public static void main(String args[]) {
        MongoClient mongoClient = (MongoClient) MongoClients.create("mongodb+srv://David:1234@cluster0.4die7u3.mongodb.net/?retryWrites=true&w=majority");
        MongoDatabase db = mongoClient.getDatabase("Pokemon");

        String collection = "pokemones";

        MongoCollection<Document> mongoCollection = db.getCollection(collection);

        // Obtener todos los documentos de la colección
        MongoCursor<Document> cursor = mongoCollection.find().iterator();

        try {
            while (cursor.hasNext()) {
                Document cliente = cursor.next();
                // Haz algo con el documento, por ejemplo imprimirlo
                System.out.println(cliente);
            }
        } finally {
            cursor.close();
        }

        // Buscar por elementos
        
        Document findDocument = new Document("against_bug", 1);

        MongoCursor<Document> resultDocument = mongoCollection.find(findDocument).iterator();

        while (resultDocument.hasNext()) {
            System.out.println(resultDocument.next().toJson());
        }
        
        // Borrar elemento
        
        Document findDocument2 = new Document("name", "Bulbasur");

        mongoCollection.findOneAndDelete(findDocument2);
        
        // Modificar elemento
        
        Document findDocument3 = new Document("name", "Bulbasur");

        Document updateDocument = new Document("$set",
        new Document("name", "Bulbasaur"));

        mongoCollection.findOneAndUpdate(findDocument3, updateDocument);
        
        
        //Crear elemento
        
        insertar(db,collection);

    }
    
    public static void insertar(MongoDatabase db, String nombre) {
        MongoCollection<Document> mCollection = db.getCollection(nombre);
        try {
                InsertOneResult result = mCollection.insertOne(new Document()
                        .append("_id", new ObjectId())
                        .append("name", "Carlos"));
                        
                System.out.println("Id del documento insertado: " + result.getInsertedId());
            } catch (MongoException me) {
                System.err.println("Error al insertar el documento: " + me);
            }
    }

    
}
