import BarraNav from './BarraNav';
import { Button } from "react-bootstrap";
import Footer from './Footer';
import imagen from './imagenes/error.jpg'

export default function Error(){
    return(
        <>
            <Button href="http://localhost:3000/" className="mt-3 mb-2" id="botonVolver" >INICIO</Button>
            <div>
                <img id="fondo" src={imagen}/>
            </div>
            <Footer nombre="David Blanch Galeano"/>
        </>
    )
}