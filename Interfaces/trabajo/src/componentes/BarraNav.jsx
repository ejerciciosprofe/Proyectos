import { Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css'
import './BarraNav.css'

export default function BarraNav(){
    let buttons = (
        <div>
            <Button href="http://localhost:3000/" className="mt-3 mb-2" id="boton1" >INICIO</Button>
            <Button href="http://localhost:3000/personajes" className="mt-3 mb-2" id="boton2">PERSONAJES</Button>
            <Button href="http://localhost:3000/favorito" className="mt-3 mb-2" id="boton3">FAVORITO</Button>
        </div>
    )
    return(
        <header>
            <nav>
                <ul>{buttons}</ul>
            </nav>
        </header>
    )
}