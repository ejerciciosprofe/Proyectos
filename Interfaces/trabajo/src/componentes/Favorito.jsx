import BarraNav from "./BarraNav";
import axios from 'axios';
import { useState, useEffect } from 'react';
import "./Favorito.css";
import Footer from "./Footer";

function Favorito() {
    const [personaje, setpersonajes] = useState([]);
    useEffect(() => {
        axios.get("http://localhost:3001/personajes/favorito").then(response => {
            setpersonajes(response.data.personaje);
        });
    }, []);

    let listpersonajes = personaje.data;

    if (personaje != null) {
        listpersonajes =
            <div id="espacioFondo">
                <div id="card">
                    <>
                        <img src={personaje.image}></img>
                        <br></br>
                        <br></br>
                        <h3>Nombre: {personaje.name}</h3>
                        <h3>Especie: {personaje.species}</h3>
                    </>
                </div>
            </div>
    }
    else {
        <h1>Erro en la conexión</h1>
    }
    return (
        <>
            <BarraNav />
            <br></br>
            {listpersonajes}
            <Footer nombre="David Blanch Galeano" />
        </>
    );

}

export default Favorito;