import BarraNav from "./BarraNav";
import axios from 'axios';
import { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { Navigate, useNavigate, useHistory } from "react-router-dom";
import imagen from './imagenes/fondoLargo4.jpg'
import Footer from "./Footer";
import "./Personajes.css";

function Personajes() {
    const history = useHistory();
    const [personaje, setpersonajes] = useState([]);
    const navigate = useNavigate()
    useEffect(() => {
        axios.get("http://localhost:3001/personajes/verPersonajes").then(response => {
            setpersonajes(response.data.personaje);
        });
    }, []);
    let listpersonajes = <h1>No hay personajes</h1>;
    if (personaje.length > 0) {

        listpersonajes = personaje.map(dato => (
                <div id="carta">
                    <>
                        <br></br>
                        <img src={dato.image}></img>
                        <br></br>
                        <h3>Nombre: {dato.name}</h3>
                        <h3>Especie: {dato.species}</h3>
                        <button id="boton" onClick={() => {
                            navigate('/');
                            window.location.href = `http://localhost:3001/personajes/eliminar/${dato.id}`;
                        }}>Eliminar</button>
                        <br></br>
                    </>
                </div>

        ));
    }
    return (
        <>
            <BarraNav />
            <br></br>
            <div id="espacioFondo2">
            {listpersonajes}
            </div>
            <Footer nombre="David Blanch Galeano" />
        </>
    );

}

export default Personajes;