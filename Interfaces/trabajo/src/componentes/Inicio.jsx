import BarraNav from './BarraNav';
import Footer from './Footer';
import imagen from './imagenes/fondo1.jpg'
import './Inicio.css'

export default function Inicio(){
    return(
        <>
            <BarraNav/>
            <div>
                <img id="fondo" src={imagen}/>
            </div>
            <Footer nombre="David Blanch Galeano"/>
        </>
    )
}