import 'bootstrap/dist/css/bootstrap.min.css'
import './Footer.css'

export default function Footer(props){
    return(
        <footer id="foot">
            <p id="texto">@ {props.nombre} SL</p>
        </footer>
    )
}