import './App.css';
import BarraNav from './componentes/BarraNav';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Favorito from './componentes/Favorito';
import Inicio from './componentes/Inicio';
import Personajes from './componentes/Personajes';
import Error from './componentes/Error';

function App() {
  return (
      <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Inicio/>}></Route>
          <Route path="/personajes" element={<Personajes/>} ></Route>
          <Route path="/favorito" element={<Favorito/>} ></Route>
          <Route path="*" element={<Error/>} ></Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
