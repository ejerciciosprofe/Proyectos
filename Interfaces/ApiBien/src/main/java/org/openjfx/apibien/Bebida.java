package org.openjfx.apibien;

public class Bebida {

    protected int id;
    protected String nombre, categoria = "",imagen;
    
    public Bebida(int id) {
        this.id = id;
    }

    public Bebida(int id, String nombre, String categoria) {
        this.id = id;
        this.nombre = nombre;
        this.categoria = categoria;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return "Bebida{" + "id=" + id + ", nombre=" + nombre + ", categoria=" + categoria + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
        
    

}
