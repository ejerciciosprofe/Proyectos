package org.openjfx.apibien;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import org.json.JSONArray;
import org.json.JSONObject;

public class PrimaryController implements Initializable {

    ArrayList<Bebida> arrayBebidas = null;
    ArrayList<String> filtro = null;

    @FXML
    private Button bAleatorio;

    @FXML
    private ComboBox<String> bPorAlcoholico;

    @FXML
    private ComboBox<String> bPorCategoria;

    @FXML
    private TextField bPorNombre;

    @FXML
    private Button buscar;

    @FXML
    private TableColumn<Bebida, String> cCategoria;

    @FXML
    private TableColumn<Bebida, Integer> cId;

    @FXML
    private TableColumn<Bebida, String> cNombre;

    @FXML
    private ImageView imagenCoctel;

    @FXML
    private ImageView malparido;

    @FXML
    private Label nombre;

    @FXML
    private TableView<Bebida> tabla;

    ObservableList<Bebida> ol = null;

    @FXML
    void accionAleatorio(ActionEvent event) throws IOException {
        String url = "https://www.thecocktaildb.com/api/json/v1/1/random.php";
        InputStream inputStream = null;
        try {
            inputStream = new URL(url).openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String response = null;
        try {
            response = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject(response);
        String fichero = jsonObject.toString();
        try {
            mostrarDatosAleatorios(fichero, "\"idDrink\":", 11, "id");
            mostrarDatosAleatorios(fichero, "\"strDrink\":", 12, "nombre");
            mostrarDatosAleatorios(fichero, "\"strDrinkThumb\":", 17, "imagen");
        } catch (Exception e) {

        }
    }

    @FXML
    void buscarCoctel(ActionEvent event) throws IOException {

        for (int i = 0; i < tabla.getItems().size(); i++) {
            tabla.getItems().clear();
        }

        arrayBebidas = new ArrayList<Bebida>();
        String url = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=" + bPorNombre.getText();
        InputStream inputStream = null;
        try {
            inputStream = new URL(url).openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String response = null;
        try {
            response = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject jsonObject = new JSONObject(response);
        String fichero = jsonObject.toString();
        getData(fichero, "\"idDrink\":", 11, "id");
        getData(fichero, "\"strDrink\":", 12, "nombre");
        getData(fichero, "\"strCategory\":", 15, "categoria");
        getData(fichero, "\"strDrinkThumb\":", 17, "imagen");

        for (Bebida i : arrayBebidas) {
            tabla.getItems().addAll(i);
        }
    }

    @FXML
    void cambiarImagen(MouseEvent event) {
        if (tabla.getSelectionModel().getSelectedItem() != null) {
            Bebida b = tabla.getSelectionModel().getSelectedItem();
            nombre.setText(b.getId() + "");
            nombre.setText(nombre.getText() + ": " + b.getNombre());
            imagenCoctel.setImage(new Image(b.getImagen()));
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cId.setCellValueFactory(new PropertyValueFactory<>("id"));
        cNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        cCategoria.setCellValueFactory(new PropertyValueFactory<>("categoria"));

        bPorCategoria.getItems().addAll("Categorias", "Glasses", "Ingredientes");
    }

    @FXML
    void filtrarElCoctel(ActionEvent event) {
        try {
            String url = null;
            for (int i = 0; i < tabla.getItems().size(); i++) {
                tabla.getItems().clear();
            }
            arrayBebidas = new ArrayList<Bebida>();
            if (bPorCategoria.getValue().equals("Categorias")) {
                url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=" + bPorAlcoholico.getValue();
            } else if (bPorCategoria.getValue().equals("Glasses")) {
                url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?g=" + bPorAlcoholico.getValue();
            } else if (bPorCategoria.getValue().equals("Ingredientes")) {
                url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=" + bPorAlcoholico.getValue();
            }

            InputStream inputStream = null;
            try {
                inputStream = new URL(url).openStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String response = null;
            try {
                response = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject jsonObject = new JSONObject(response);
            String fichero = jsonObject.toString();
            getData(fichero, "\"idDrink\":", 11, "id");
            getData(fichero, "\"strDrink\":", 12, "nombre");
            getData(fichero, "\"strDrinkThumb\":", 17, "imagen");

            for (Bebida i : arrayBebidas) {
                tabla.getItems().addAll(i);
            }
        } catch (Exception e) {

        }

    }

    void mostrarDatosAleatorios(String fichero, String dato, int i, String tipo) {
        fichero = fichero.substring(fichero.indexOf(dato) + i);
        if (tipo.equalsIgnoreCase("id")) {
            nombre.setText((fichero.substring(0, fichero.indexOf("\""))));
        } else if (tipo.equalsIgnoreCase("nombre")) {
            nombre.setText(nombre.getText() + ": " + (fichero.substring(0, fichero.indexOf("\""))));
        } else if (tipo.equalsIgnoreCase("imagen")) {
            imagenCoctel.setImage(new Image(fichero.substring(0, fichero.indexOf("\""))));
        }
    }

    @FXML
    void cambiarCategoria(ActionEvent event) {
        try {

            filtro = new ArrayList<String>();

            if (bPorCategoria.getValue().equals("Categorias")) {
                String url = "https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list";
                InputStream inputStream = null;
                try {
                    inputStream = new URL(url).openStream();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                String response = null;
                try {
                    response = br.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                JSONObject jsonObject = new JSONObject(response);
                String fichero = jsonObject.toString();
                getFiltros(fichero, "\"strCategory\":", 15);
            } else if (bPorCategoria.getValue().equals("Glasses")) {
                String url = "https://www.thecocktaildb.com/api/json/v1/1/list.php?g=list";
                InputStream inputStream = null;
                try {
                    inputStream = new URL(url).openStream();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                String response = null;
                try {
                    response = br.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                JSONObject jsonObject = new JSONObject(response);
                String fichero = jsonObject.toString();
                getFiltros(fichero, "\"strGlass\":", 12);
            } else if (bPorCategoria.getValue().equals("Ingredientes")) {
                String url = "https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list";
                InputStream inputStream = null;
                try {
                    inputStream = new URL(url).openStream();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                String response = null;
                try {
                    response = br.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                JSONObject jsonObject = new JSONObject(response);
                String fichero = jsonObject.toString();
                getFiltros(fichero, "\"strIngredient1\":", 18);
            }

            bPorAlcoholico.getItems().clear();

            for (String i : filtro) {
                bPorAlcoholico.getItems().addAll(i);
            }
        } catch (Exception e) {

        }
    }

    void getFiltros(String fichero, String dato, int i) {
        while (fichero.indexOf(dato) != -1) {
            fichero = fichero.substring(fichero.indexOf(dato) + i);
            filtro.add(fichero.substring(0, fichero.indexOf("\"")));
        }
    }

    void getData(String fichero, String cut, int length, boolean control) {
        fichero = fichero.substring(fichero.indexOf(cut) + length);
        imagenCoctel.setImage(new Image(fichero.substring(0, fichero.indexOf("\""))));
    }

    private void getData(String fichero, String dato, int i, String tipo) {
        int cont = 0;
        while (fichero.indexOf(dato) != -1) {
            fichero = fichero.substring(fichero.indexOf(dato) + i);
            if (tipo.equalsIgnoreCase("id")) {
                arrayBebidas.add(new Bebida(Integer.parseInt(fichero.substring(0, fichero.indexOf("\"")))));
            } else if (tipo.equalsIgnoreCase("nombre")) {
                arrayBebidas.get(cont).setNombre(fichero.substring(0, fichero.indexOf("\"")));
                cont++;
            } else if (tipo.equalsIgnoreCase("categoria")) {
                arrayBebidas.get(cont).setCategoria(fichero.substring(0, fichero.indexOf("\"")));
                cont++;
            } else if (tipo.equalsIgnoreCase("imagen")) {
                arrayBebidas.get(cont).setImagen((fichero.substring(0, fichero.indexOf("\""))));
                cont++;
            }
        }
    }
}
